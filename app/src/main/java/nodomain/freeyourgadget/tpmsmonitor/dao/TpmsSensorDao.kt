package nodomain.freeyourgadget.tpmsmonitor.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import nodomain.freeyourgadget.tpmsmonitor.model.TpmsSensor

@Dao
interface TpmsSensorDao {

    @Query("Select * from tpmssensor")
    fun getAll(): LiveData<List<TpmsSensor>>

    @Query("SELECT * FROM tpmssensor WHERE mac_address IN (:macs)")
    fun loadAllByIds(macs: Array<String>): LiveData<List<TpmsSensor>>

    @Query("SELECT * FROM tpmssensor WHERE bt_name LIKE :btName LIMIT 1")
    fun findByName(btName: String): TpmsSensor

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(tpmsSensor: TpmsSensor)

    @Delete
    fun delete(tpmsSensor: TpmsSensor)
}