package nodomain.freeyourgadget.tpmsmonitor.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import nodomain.freeyourgadget.tpmsmonitor.model.TpmsReading

@Dao
interface TpmsReadingDao {

    //https://medium.com/androiddevelopers/7-pro-tips-for-room-fbadea4bfbd1

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertReading(tpmsReading: TpmsReading);

    @Query("Select * from tpmsreading order by timestamp DESC")
    fun loadAllReadings(): LiveData<List<TpmsReading>>

    @Query("Select * from tpmsreading order by timestamp DESC limit :n")
    fun loadLastNReadings(n: Int): LiveData<List<TpmsReading>>

    @Query("Select * from tpmsreading where mac_address like :mac order by timestamp DESC")
    fun loadAllReadingsbysensor(mac: String): LiveData<List<TpmsReading>>

    @Query("Select * from tpmsreading where mac_address like :mac order by timestamp DESC LIMIT 1")
    fun loadLastReadingsbysensor(mac: String): LiveData<List<TpmsReading>>
}