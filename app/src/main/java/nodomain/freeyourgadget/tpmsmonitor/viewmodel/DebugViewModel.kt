package nodomain.freeyourgadget.tpmsmonitor.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import nodomain.freeyourgadget.tpmsmonitor.model.TpmsReading

class DebugViewModel:ViewModel() {
    val items: LiveData<List<TpmsReading>> = MutableLiveData<List<TpmsReading>>().apply {}
}