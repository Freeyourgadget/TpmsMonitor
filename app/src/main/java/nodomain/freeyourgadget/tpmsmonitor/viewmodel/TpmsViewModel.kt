package nodomain.freeyourgadget.tpmsmonitor.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import nodomain.freeyourgadget.tpmsmonitor.dao.TpmsReadingDao
import nodomain.freeyourgadget.tpmsmonitor.dao.TpmsSensorDao
import nodomain.freeyourgadget.tpmsmonitor.db.AppDatabase
import nodomain.freeyourgadget.tpmsmonitor.model.TpmsReading
import nodomain.freeyourgadget.tpmsmonitor.model.TpmsSensor
import java.util.concurrent.Executors

class TpmsViewModel(application: Application) : AndroidViewModel(application) {

    private val tpmsReadingDao: TpmsReadingDao
    private val tpmsSensorDao: TpmsSensorDao
    private val tpmsSensorLiveData: LiveData<List<TpmsSensor>>
    val tpmsReadingLiveData: LiveData<List<TpmsReading>>

    init {
        val appDatabase = AppDatabase.getDatabase(application)
        tpmsReadingDao = appDatabase.TpmsReadingDao()
        tpmsSensorDao = appDatabase.TpmsSensorDao()

        tpmsReadingLiveData = tpmsReadingDao.loadAllReadings()
        tpmsSensorLiveData = tpmsSensorDao.getAll()
    }


    fun insertTpmsSensor(tpmsSensor: TpmsSensor) {
        Executors.newSingleThreadExecutor().execute {
            tpmsSensorDao.insert(tpmsSensor)
        }
    }

    fun insertTpmsReading(tpmsReading: TpmsReading) {
        Executors.newSingleThreadExecutor().execute {
            tpmsReadingDao.insertReading(tpmsReading)
        }
    }

}