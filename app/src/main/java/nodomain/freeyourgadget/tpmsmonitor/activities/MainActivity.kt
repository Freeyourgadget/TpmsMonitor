package nodomain.freeyourgadget.tpmsmonitor.activities

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.core.content.PermissionChecker
import androidx.recyclerview.widget.RecyclerView
import nodomain.freeyourgadget.tpmsmonitor.services.BackgroundScanner
import nodomain.freeyourgadget.tpmsmonitor.R
import nodomain.freeyourgadget.tpmsmonitor.fragments.DebugReadings.OnFragmentInteractionListener


//https://developer.android.com/topic/libraries/architecture/viewmodel

class MainActivity : AppCompatActivity(), OnFragmentInteractionListener {

    override fun onFragmentInteraction(uri: Uri) {
        //only here to make navigation component happy?
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.main_activity)

        val intent = Intent(this@MainActivity, BackgroundScanner::class.java)

        if (PermissionChecker.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            startService(intent)
        else
            requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), 1)

        startService(intent)
    }
}
