package nodomain.freeyourgadget.tpmsmonitor.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.android.synthetic.main.main_fragment.view.*
import nodomain.freeyourgadget.tpmsmonitor.R
import nodomain.freeyourgadget.tpmsmonitor.viewmodel.TpmsViewModel

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: TpmsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        val view = inflater.inflate(R.layout.main_fragment, container, false)

        view.button.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_mainFragment_to_debugReadings, null))

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(TpmsViewModel::class.java)
        // TODO: Use the ViewModel




    }

}
