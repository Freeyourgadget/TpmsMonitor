# TpmsMonitor

This app allows to parse, show and store pressure and temperature as received by bluetooth low energy tire pressure
monitor sensors (TPMS).

These sensors do not allow to establish a connection to other devices, but rather broadcast the readings as they see
more fit (usually when a change in pressure or temperature is detected), requiring companion apps to just be listening
for incoming messages.

## Plans for the future
In future releases the sensors will be associated with vehicles and wheels within the app, to get a more useful overview
so far there is only a fragment showing all readings in the database, the list is updated live as soon as new readings
come in.

## Development principles
This application requires the minimum set of permissions needed to interact with other bluetooth devices (unfortunately
acquiring the device location is needed), further it is a playground for me (the developer) to learn about the newly
added android features (like data binding, room, etc.) and my first experience with Kotlin as a programming language.